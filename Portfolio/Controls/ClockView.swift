///// Copyright (c) 2018 Razeware LLC
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
/// 
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit

@IBDesignable
class ClockView: UIView {
  
  let clockLayer = CAShapeLayer()
  let arrowLayer = CAShapeLayer()
  let numbersLayer = CALayer()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setup()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    animatePointer()

  }
  
  override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    setup()
  }

  private func setup() {
    drawNumbers()
    layer.addSublayer(clockLayer)
    layer.addSublayer(numbersLayer)
    layer.addSublayer(arrowLayer)
    setupClock()
    setupArrow()
  }
  
  fileprivate func drawNumbers() {
    numbersLayer.bounds = bounds
    numbersLayer.position = CGPoint(x: bounds.midX, y: bounds.midY)
    
    let renderer = UIGraphicsImageRenderer(size: bounds.size)
    let image = renderer.image{ context in
      let cgContext = context.cgContext
      cgContext.saveGState()
      for i in 1...12 {
        cgContext.translateBy(x: bounds.midX, y: bounds.midY)
        cgContext.rotate(by: CGFloat.pi/6)
        cgContext.translateBy(x: -bounds.midX, y: -bounds.midY)
        draw(number: i)
      }
      cgContext.restoreGState()
    }
    numbersLayer.contents = image.cgImage
  }
  
  fileprivate func setupClock(){
    let width = bounds.width
    let path = UIBezierPath(ovalIn: bounds)
    clockLayer.lineWidth = 1
    clockLayer.strokeColor = UIColor.black.cgColor
    clockLayer.fillColor = UIColor.white.cgColor
    clockLayer.shadowOpacity = 0.3
    clockLayer.shadowRadius = 5
    clockLayer.shadowOffset = CGSize.zero
    clockLayer.path = path.cgPath
  }
  
  fileprivate func setupArrow(){
    let width = bounds.width
    let path = UIBezierPath()
    path.move(to: CGPoint.zero)
    path.addLine(to: CGPoint(x: 0, y: bounds.width * 0.3 ))
    arrowLayer.path = path.cgPath
    arrowLayer.lineWidth = 3
    arrowLayer.lineCap = kCALineCapRound
    arrowLayer.strokeColor = UIColor.black.cgColor
    arrowLayer.position = CGPoint(x: bounds.midX, y: bounds.midY)

  }
  
  fileprivate func animatePointer(){
    let animation = CABasicAnimation(keyPath: "transform.rotation.z")
    animation.duration = 60
    animation.fromValue = 0
    animation.toValue = Float.pi * 2
    animation.repeatCount = .greatestFiniteMagnitude
    self.arrowLayer.add(animation, forKey: "time")
  }
  

  // method for drawing the numbers in section 2
  func draw(number: Int) {
    let string = "\(number)" as NSString
    let attributes = [NSAttributedStringKey.font: UIFont(name: "Avenir-Heavy", size: 18)!]
    let size = string.size(withAttributes: attributes)
    string.draw(at: CGPoint(x: bounds.width/2 - size.width/2, y: 10), withAttributes: attributes)
    
  }

  
}

